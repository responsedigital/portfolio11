<!-- Start Portfolio11 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A logo wall with links for each -->
@endif
<div class="portfolio-11" is="fir-portfolio-11" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="portfolio-11__wrap">
        @foreach($logos as $logo)

            @if($logo['link'])
                <a href="{{ $logo['link'] }}" class="portfolios-11__logo">
                    <img src="{{ $logo['image']['url'] }}" alt="{{ $logo['image']['alt'] }}">
                </a>
            @else 
                <span class="portfolios-11__logo">
                    <img src="{{ $logo['image']['url'] }}" alt="{{ $logo['image']['alt'] }}">
                </span>
            @endif

        @endforeach
  </div>
</div>
<!-- End Portfolio11 -->