class Portfolio11 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initPortfolio11()
    }

    initPortfolio11 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Portfolio11")
    }

}

window.customElements.define('fir-portfolio-11', Portfolio11, { extends: 'div' })
