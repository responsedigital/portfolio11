<?php

namespace Fir\Pinecones\Portfolio11;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Portfolio11',
            'label' => 'Pinecone: Portfolio11',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A logo wall with links for each"
                ],
                [
                    'label' => 'Logos',
                    'name' => 'logoTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => 'Logos',
                    'name' => 'logos',
                    'type' => 'repeater',
                    'layout' => 'table',
                    'min' => 1,
                    'button_label' => 'Add another Logo',
                    'sub_fields' => [
                        [
                            'label' => 'Image',
                            'name' => 'image',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Link',
                            'name' => 'link',
                            'type' => 'url'
                        ],
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
